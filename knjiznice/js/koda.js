
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";
  sessionId = getSessionId();
  jQuery('#ustvarjenId').html('');

  if (stPacienta == 1) {
    var ime = "Jože";
    var priimek = "Potrebuješ";
    var datumRojstva = "1980-11-21T11:40Z";

    var datumInUraSeznam = ["2007-01-01","2008-01-01","2009-01-01","2010-01-01","2011-01-01","2012-01-01","2013-01-01","2014-01-01","2015-01-01","2016-01-01"];
    var telesnaVisina = "175";
    var telesnaTezaSeznam = ["65","70","74","78","80","83","82","85","87","90"];
  }
  else if (stPacienta == 2) {
    var ime = "Andrej";
    var priimek = "Kovač";
    var datumRojstva = "2000-10-15T15:20Z";
    var merilec = "Andrej";

    var datumInUraSeznam = ["2007-01-01","2008-01-01","2009-01-01","2010-01-01","2011-01-01","2012-01-01","2013-01-01","2014-01-01","2015-01-01","2016-01-01"];
    var telesnaVisina = "180";
    var telesnaTezaSeznam = ["78","74","71","70","67","64","62","61","60","60"];
    var merilec = "Nejc";
  }
  else {
    var ime = "Maja";
    var priimek = "Kosovel";
    var datumRojstva = "1990-05-21T09:35Z";

    var datumInUraSeznam = ["2007-01-01","2008-01-01","2009-01-01","2010-01-01","2011-01-01","2012-01-01","2013-01-01","2014-01-01","2015-01-01","2016-01-01"];
    var telesnaVisina = "165";
    var telesnaTezaSeznam = ["50","57","54","55","56","57","59","60","60","58"];
    var merilec = "Nika";
  }

  $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function (data) {
            var ehrId = data.ehrId;
            ehrId = data.ehrId;
            var partyData = {
                firstNames: ime,
                lastNames: priimek,
                dateOfBirth: datumRojstva,
                partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                      var element = document.getElementById("ustvarjenId");
                      element.insertAdjacentHTML('beforeend', "<span class='obvestilo " +
                          "label label-success fade-in'>"+ ime + " " + priimek + ": " +
                          ehrId + "</span><br>");
                      //console.log(ehrId);


                      for (var i = 0; i < 10; i++) {
                        var datumInUra = datumInUraSeznam[i];
                        var telesnaTeza = telesnaTezaSeznam[i];
                        $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });
    var podatki = {
      // Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": datumInUra,
        "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
        "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    };
    var parametriZahteve = {
        ehrId: ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: merilec
    };
    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(podatki),
        success: function (res) {
            //console.log("Uspešno dodajanje");
        },
        error: function(err) {
          console.log("Neuspešno dogajanje");
        }
    });


                      }


                      return ehrId;
                        //$("#ustvarjenId").html("<span class='obvestilo " +
                          //"label label-success fade-in'>"+ ime + priimek +
                          //ehrId + "</span>");
                        //$("#preberiEHRid").val(ehrId);
                    }
                },
                error: function(err) {
                  console.log("Napaka");
                }
            });
        }
    });
  return true;
}

var tabelaVisin = [];
var tabelaTez = [];
var tabelaLet = [];
var tabelaBMI = [];

function prikaziInIzracunaj() {
  tabelaTez = [];
  tabelaVisin = [];
  tabelaLet = [];
  tabelaBMI = [];
jQuery('#rezultatMeritevTeze').html('');
jQuery('#rezultatMeritevVisine').html('');
jQuery('#rezultatIzracunaBMI').html('');
sessionId = getSessionId();

var ehrId = $("#zeljeniId").val();

$.ajax({
  url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
  async: false,
  type: 'GET',
  headers: {"Ehr-Session": sessionId},
  success: function (data) {
    var party = data.party;
    //$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
        //"podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
        //" " + party.lastNames + "'</b>.</span><br/><br/>");

$.ajax({
  url: baseUrl + "/view/" + ehrId + "/" + "weight",
  async: false,
  type: 'GET',
  headers: {"Ehr-Session": sessionId},
  success: function (res) {
    if (res.length > 0) {
      var results = "<table class='table table-striped " +
            "table-hover'><tr><th>Datum in ura</th>" +
            "<th class='text-right'>Telesna teža</th></tr>";
      for (var i in res) {
        tabelaTez.push(res[i].weight);/////////////////////////////////
        tabelaLet.push(res[i].time.substr(0,4));
        results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " "  +
                          res[i].unit + "</td></tr>";
      }
      results += "</table>";
      $("#rezultatMeritevTeze").append(results);
    } else {
      console.log("Ni podatkov");
    }
  },
  error: function() {
    console.log("Napaka");
  }
});
  },
  error: function(err) {
        console.log("Napaka");
        }
});
$.ajax({
  url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
  async: false,
  type: 'GET',
  headers: {"Ehr-Session": sessionId},
  success: function (data) {
    var party = data.party;
    //$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
        //"podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
        //" " + party.lastNames + "'</b>.</span><br/><br/>");

$.ajax({
  url: baseUrl + "/view/" + ehrId + "/" + "height",
  async: false,
  type: 'GET',
  headers: {"Ehr-Session": sessionId},
  success: function (res) {
    if (res.length > 0) {
      var results = "<table class='table table-striped " +
            "table-hover'><tr><th>Datum in ura</th>" +
            "<th class='text-right'>Telesna višina</th></tr>";
      for (var i in res) {
        tabelaVisin.push(res[i].height/100);
        results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].height + " "  +
                          res[i].unit + "</td></tr>";
      }
      results += "</table>";
      $("#rezultatMeritevVisine").append(results);
    } else {
      console.log("Ni podatkov");
    }
  },
  error: function() {
    console.log("Napaka");
  }
});
  },
  error: function(err) {
        console.log("Napaka");
        }
});
var results = "<table class='table table-striped " +
            "table-hover'><tr><th>Leto</th>" +
            "<th class='text-right'>BMI</th></tr>";
for (var i = 0; i < tabelaVisin.length; i++) {
  var rezultat = (tabelaTez[i]/(tabelaVisin[i]*tabelaVisin[i]));
  rezultat = rezultat.toFixed(1);
  tabelaBMI[i] = Number(rezultat);
  results += "<tr><td>" + tabelaLet[i] +"</td><td class='text-right'>" + rezultat + "</td></tr>";
}
results += "</table>";
$("#rezultatIzracunaBMI").append(results);

tabelaBMI.reverse();
//tabelaTez.reverse();
//console.log(tabelaVisin);
//console.log(tabelaBMI);
}

var BMIevropa = [];
var BMIamerika = [];

$.getJSON("https://bitbucket.org/jakec125/jakec125.bitbucket.org/raw/master/dataVseSkupaj.json", function(data) {
  
  var yourval = jQuery.parseJSON(JSON.stringify(data));
  for (var i in data["fact"]) {
    if (data["fact"][i]["dims"]["REGION"] == "Europe") {
      BMIevropa.push(Number(data["fact"][i]["Value"].substr(0,4)));
    }
    else {
      BMIamerika.push(Number(data["fact"][i]["Value"].substr(0,4)));
    }
    
  }
  BMIevropa.reverse();
  //console.log(BMIevropa);
});

function narisiGraf() {
  
  jQuery('#chart').html('');
  var podatki1 = ['Bolnik'];
  podatki1 = podatki1.concat(tabelaBMI);
  var podatki2 = ['Evropsko povprečje'];
  podatki2 = podatki2.concat(BMIevropa);
  var podatki3 = ['Ameriško povprečje'];
  podatki3 = podatki3.concat(BMIamerika);
  var chart = c3.generate({
    bindto: '#chart',
    data: {
      x: 'x',
      columns: [
        ['x', 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016],
        podatki1,
        podatki2,
        podatki3
      ]
    },
    
    axis: {
      x: {
        label: {
          text: 'Leto',
          position: 'outer-center'
        }
      },
      y: {
        label: 'BMI',
        position: 'outer-middle'
      }
    }
});
}

$(document).ready(function() {
  $('#preberiFiksniEhrId').change(function() {
    //$("#preberiSporocilo").html("");
    $("#zeljeniId").val($(this).val());
  });


});